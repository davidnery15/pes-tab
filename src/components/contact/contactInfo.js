import React from 'react'
import { Div, P } from '../styled-components'
import {
  addressEastOffice1,
  addressEastOffice2,
  phoneEastOffice1,
  phoneEastOffice2,
  emailEastOffice,
  addressWestOffice1,
  addressWestOffice2,
  phoneWestOffice,
  emailWestOffice,
} from '../config'

const ContactInfo = () => (
  <Div
    padding="40px 10px"
    display="flex"
    flexDirection="column"
    align_items="center"
    background_color="#f2f2f2"
  >
    <Div
      width="345px"
      width960="275px"
      display="flex"
      flexDirection="column"
    >
      <P
        margin="0"
        fontSize="24px"
        fontWeight="bold"
        color="#666"
      >
        Premier Energy, Inc (East Office)
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        {addressEastOffice1}
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        {addressEastOffice2}
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        Phone Number: {phoneEastOffice1}
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        Alternate Number: {phoneEastOffice2}
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        Email: {emailEastOffice}
      </P>
    </Div>
    <Div
      margin="30px 0 0"
      width="345px"
      width960="275px"
      display="flex"
      flexDirection="column"
    >
      <P
        margin="0"
        fontSize="24px"
        fontWeight="bold"
        color="#666"
      >
        Premier Energy, Inc (West Office)
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        {addressWestOffice1}
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        {addressWestOffice2}
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        Phone Number: {phoneWestOffice}
      </P>
      <P
        margin="0"
        fontSize="20px"
        color="#666"
      >
        Email: {emailWestOffice}
      </P>
    </Div>
  </Div>
)

export default ContactInfo