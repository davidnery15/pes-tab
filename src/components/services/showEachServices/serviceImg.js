import React from 'react'
import { Div, P } from '../../styled-components'

const ServiceImg = () => (
  <Div
    width="500px"
    height="300px"
    width960="280px"
    height960="220px"
    background_color="#666"
    display="flex"
    align_items="center"
    justify_content="center"
  >
    <P
      margin="0"
      fontSize="22px"
      fontWeight="bold"
      color="#fff"
    >
      FOTO AQUÍ
    </P>
  </Div>
)

export default ServiceImg