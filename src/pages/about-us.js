import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import GeneralBanner from '../components/generalBanner'
import ServiceProvided from '../components/aboutUs/serviceProvided'
import Goals from '../components/aboutUs/goals'
import Certification from '../components/certification'

const AboutUs = () => (
  <Layout>
    <SEO title="About Us" />
    <GeneralBanner
      title="ABOUT US"
    />
    <ServiceProvided />
    <Goals />
    <Certification
      background="#f2f2f2"
    />
  </Layout>
)

export default AboutUs
