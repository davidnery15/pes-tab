import Typography from "typography"

const typography = new Typography({
  headerFontFamily: ['Calibri'],
  bodyFontFamily: ['Calibri']
})

export const { scale, rhythm, options } = typography
export default typography